export interface INode {
  id: number;
  name: string;
  x: number;
  y: number;
  parentId?: number;
  type: string;
  label: string;
  children?: INode[];
}

export interface ILink {
  id?: number;
  from: number;
  to: number;
}

export interface ILayout {
  nodes: INode[];
  centerX: number;
  centerY: number;
  scale: number;
  links: ILink[];
}
